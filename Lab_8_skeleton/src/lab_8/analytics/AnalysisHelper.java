/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab_8.analytics;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import static java.util.Collections.list;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import lab_8.entities.Comment;
import lab_8.entities.Post;
import lab_8.entities.User;

/**
 *
 * @author harshalneelkamal
 */
public class AnalysisHelper {
    
    
    public void userWithMostLikes(){
        Map<Integer, Integer> userLikecount = new HashMap<>();
        //没有new一个对象，而是直接get一个对象
        Map<Integer, User> users = DataStore.getInstance().getUsers();
        for(User user : users.values()){
            for(Comment c : user.getComments()){
                int likes = 0;
                if(userLikecount.containsKey(user.getId()))
                    likes +=c.getLikes();
                userLikecount.put(user.getId(), likes);
            }
        }
        
        int max = 0;
        int maxId = 0;
        for(int id : userLikecount.keySet()){
            if(userLikecount.get(id) > max){
                max = userLikecount.get(id);
                maxId = id;
            }
        }
        
        System.out.println("User with most likes : " +max+"\n"+users.get(maxId));
        
    }
    
    public void getFiveMostLikedComment(){
        Map<Integer, Comment> comments = DataStore.getInstance().getComments();
        List<Comment> commentList = new ArrayList<>(comments.values());
        Collections.sort(commentList, new Comparator<Comment>(){
            public int compare(Comment o1, Comment o2){
                return o2.getLikes() - o1.getLikes();
            }  
    });
        
        System.out.println("5 most liked commits :");
        for(int i = 0; i < commentList.size() && i < 5; i++){
            System.out.println(commentList.get(i));
        }
        
    }
    
    //lyu
    public void findAverageLikesPerComment(){
        Map<Integer, Comment> comments = DataStore.getInstance().getComments();
        int likes = 0;
        for(Comment c : comments.values()){
            likes += c.getLikes();
        }
        int avarageLikes = likes/comments.size();
        System.out.println("Average likes per comment: " +avarageLikes);
    }
    
    //lyu
    public void PostWithMostLikeComment(){
        Map<Integer, Post> posts = DataStore.getInstance().getPosts();
        Map<Integer, Integer> postLikecount = new HashMap<>();
        for(Post post : posts.values()){
            for(Comment c : post.getComments()){
                int likes = 0;
                if(postLikecount.containsKey(post.getPostId()))
                    likes +=c.getLikes();
                postLikecount.put(post.getPostId(), likes);
            }
        }
        
        int max = 0;
        int maxId = 0;
        for(int id : postLikecount.keySet()){
            if(postLikecount.get(id) > max){
                max = postLikecount.get(id);
                maxId = id;
            }
        }
        
        System.out.println("Post with most likes comment: " +max+"\n"+posts.get(maxId));
            
        
    }
    
    //lyu
    public void PostWithMostComment(){
        Map<Integer, Post> posts = DataStore.getInstance().getPosts();
        Map<Integer, Integer> postCommentcount = new HashMap<>();
        for(Post post : posts.values()){
            List<Comment> commentList = post.getComments();
            int commentCount = commentList.size();
            postCommentcount.put(post.getPostId(), commentCount);
            }
        int max = 0;
        int maxId = 0;
        for(int id : postCommentcount.keySet()){
            if(postCommentcount.get(id) > max){
                max = postCommentcount.get(id);
                maxId = id;
            }
        }
        System.out.println("Post with most comments: " +max+"\n"+posts.get(maxId));
    }
    
    //wly
    public void PostWithTop5InactiveUsersBasedPost(){
       Map<Integer, Post> posts = DataStore.getInstance().getPosts();
       Map<Integer, User> users = DataStore.getInstance().getUsers();
       Map<Integer, Integer> userPostCount = new HashMap<>();
       
       //post里有userId 遍历整个post 如果有userId一样的 userPostCount值++
       int userPostCountValue = 1;
       for(Post post : posts.values()){
           for(User user: users.values())
            if(post.getUserId() == user.getId()){
                if(userPostCount.get(user.getId())==null){
                    userPostCount.put(user.getId(), userPostCountValue);
                }else{
                   int value = userPostCount.get(user.getId());
                   value++;
                   userPostCount.replace(user.getId(), value);
                }    
            }  
        }
       
       //排序
        List<Map.Entry<Integer,Integer>> userPostCountList=new ArrayList<>();

        userPostCountList.addAll(userPostCount.entrySet());
        ValueComparator vc=new ValueComparator();
        Collections.sort(userPostCountList,vc);

        List<User> userList = new ArrayList<>();

        for(Map.Entry<Integer,Integer> user : userPostCountList){
            userList.add(users.get(user.getKey()));

        }
        
        System.out.println(" Verify: top 5 inactice users based on posts:");
        for(int i = 0; i < userPostCountList.size() && i < 5; i++){
            System.out.println("UserID = "+userPostCountList.get(i).getKey()+" Have "+userPostCountList.get(i).getValue()+" POST"); 
        }
        
        System.out.println(" top 5 inactice users based on posts:");
        for(int i = 0; i < userList.size() && i < 5; i++){
            System.out.println(userList.get(i)); 
        }

       
    }
    
    //wly
    public void PostWithTop5InactiveUsersBasedCommit(){
        Map<Integer, User> users = DataStore.getInstance().getUsers();
        List<User> userList = new ArrayList<>(users.values());
        Collections.sort(userList, new Comparator<User>(){
            public int compare(User u1, User u2){
                return u1.getComments().size() - u2.getComments().size();
            }  
        });
        
        System.out.println("5 inactive users based on commits :");
        for(int i = 0; i < userList.size() && i < 5; i++){
            System.out.println(userList.get(i));
        }
        
        
    }
    
    //cyj
    public void getFiveInactiveUser_Overall(){
        Map<Integer, Post> posts = DataStore.getInstance().getPosts();
        Map<Integer, Integer> overallCount = new HashMap<>();
       
        for(Post post : posts.values()){
            for(Comment comment :post.getComments()){
                int commentUserId = comment.getUserId();
                if(!overallCount.containsKey(commentUserId) ){
                    overallCount.put(commentUserId, 1);
                    
                }else{
                    overallCount.replace(commentUserId, overallCount.get(commentUserId) + 1);
                }
                if(!overallCount.containsKey(post.getUserId()) ){
                    overallCount.put(post.getUserId(), 1);
                }else{
                    overallCount.replace(post.getUserId(), overallCount.get(post.getUserId()) + 1);
                }
            }
        }
        
        //排序
        List<Map.Entry<Integer,Integer>> overAllCountList=new ArrayList<>();

        overAllCountList.addAll(overallCount.entrySet());
        ValueComparator vc=new ValueComparator();
        Collections.sort(overAllCountList,vc);

        List<Post> overAllList = new ArrayList<>();

        for(Map.Entry<Integer,Integer> overAll : overAllCountList){
            overAllList.add(posts.get(overAll.getKey()));

        }
        
        System.out.println(" Top 5 inactice users based on Over All");
        for(int i = 0; i < overAllCountList.size() && i < 5; i++){
            System.out.println("UserID = "+overAllCountList.get(i).getKey()+" Have "+overAllCountList.get(i).getValue()); 
        }
        
        
    
    }
    
    //cyj
    public void getFiveProactiveUser_Overall(){
           Map<Integer, Post> posts = DataStore.getInstance().getPosts();
        Map<Integer, Integer> overallCount = new HashMap<>();
       
        for(Post post : posts.values()){
            for(Comment comment :post.getComments()){
                int commentUserId = comment.getUserId();
                if(!overallCount.containsKey(commentUserId) ){
                    overallCount.put(commentUserId, 1);
                    
                }else{
                    overallCount.replace(commentUserId, overallCount.get(commentUserId) + 1);
                }
                if(!overallCount.containsKey(post.getUserId()) ){
                    overallCount.put(post.getUserId(), 1);
                }else{
                    overallCount.replace(post.getUserId(), overallCount.get(post.getUserId()) + 1);
                }
            }
        }
        
        //排序
        List<Map.Entry<Integer,Integer>> overAllCountList=new ArrayList<>();

        overAllCountList.addAll(overallCount.entrySet());
        ValueComparator vc=new ValueComparator();
        Collections.sort(overAllCountList,vc);

        List<Post> overAllList = new ArrayList<>();

        for(Map.Entry<Integer,Integer> overAll : overAllCountList){
            overAllList.add(posts.get(overAll.getKey()));

        }
        
        System.out.println(" Top 5 proactice users based on Over All");
        for(int i = overAllCountList.size() -1; i >=0  && i > overAllCountList.size() - 6; i--){
            System.out.println("UserID = "+overAllCountList.get(i).getKey()+" Have "+overAllCountList.get(i).getValue()); 
        }
    }
    
    private class ValueComparator implements Comparator<Map.Entry<Integer,Integer>>  
     {  
         public int compare(Map.Entry<Integer, Integer> mp1, Map.Entry<Integer, Integer> mp2)   
         {  
             return mp1.getValue() - mp2.getValue();  
         }  
     }
}
