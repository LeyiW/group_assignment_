/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Hall;

import java.util.ArrayList;

/**
 *
 * @author lvyan
 */
public class HallDirectory {
    private ArrayList<Hall> hallList;
    
    public HallDirectory() {
        hallList = new ArrayList<Hall>();
    }

    public ArrayList<Hall> getHallList() {
        return hallList;
    }
    
    public Hall createHall(Hall.HallType type, String name, int floor, int row, int column){
        Hall hall = null;
        if (type.getValue().equals(Hall.HallType.IMAX.getValue())){
            hall = new IMAXHall(name, floor, row, column,String.valueOf(type));
            hallList.add(hall);
        }
        else if (type.getValue().equals(Hall.HallType.ThreeD.getValue())){
            hall = new ThreeDHall(name, floor, row, column,String.valueOf(type));
            hallList.add(hall);
        }
        else if(type.getValue().equals(Hall.HallType.TwoD.getValue())){
            hall = new TwoDHall(name, floor, row, column,String.valueOf(type));
            hallList.add(hall);
        }
        return hall;
    }
}
