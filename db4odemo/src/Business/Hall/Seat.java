/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Hall;

/**
 *
 * @author lvyan
 */
public class Seat {
    
    private int columnNumber;
    private int rowNumber;
    private boolean available;
   

    public int getColumnNumber() {
        return columnNumber;
    }

    public void setColumnNumber(int columnNumber) {
        this.columnNumber = columnNumber;
    }

    public int getRowNumber() {
        return rowNumber;
    }

    public void setRowNumber(int rowNumber) {
        this.rowNumber = rowNumber;
    }

    public boolean isAvailable() {
        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }
    
    public Seat(int rowNumber, int columnNumber){
        this.columnNumber = columnNumber;
        this.rowNumber = rowNumber;
        this.available = true;
    }
    
    
    @Override
    public String toString(){
        return String.valueOf("row :" + (rowNumber + 1) + "column :" + (columnNumber + 1));
    }
    

}
