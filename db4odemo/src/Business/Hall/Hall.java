/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Hall;

import java.util.ArrayList;

/**
 *
 * @author lvyan
 */
public abstract class Hall {
    
    private int id;
    private String name;
    private ArrayList<Seat> seatList;
    private Seat seat;
    private String type;

    public String getType() {
        return type;
    }
    private int floor;
    private static int count = 1;

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public ArrayList<Seat> getSeatList() {
        return seatList;
    }

    public Seat getSeat() {
        return seat;
    }

    public int getFloor() {
        return floor;
    }
    
    
    public Hall(String name, int floor, int row, int column, String type){
        this.name = name;
        this.floor = floor;
        this.id = count;
        this.type = type;
        count++;
        seatList = new ArrayList<Seat>();
        for(int i = 0; i < row; i++){
            for(int j = 0; j < column; j++){
                seat = new Seat(i,j);
                seatList.add(seat);
            }
        }
    }
    
    public enum HallType{
        //Admin("Admin Organization"), Purchaser("Doctor Organization"), Dispatcher("Lab Organization");
        ThreeD("3D Hall"), IMAX("IMAX Hall"), TwoD("2D Hall");
        private String value;
        
        //constructor
        private HallType(String value) {
            this.value = value;
        }
        
        public String getValue() {
            return value;
        }
    }
    
    
    public String toString(){
        return name;
    }
}
