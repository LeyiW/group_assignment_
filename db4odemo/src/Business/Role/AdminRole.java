/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Role;

import Business.EcoSystem;
import Business.Enterprise.Enterprise;
import Business.Organization.Organization;
import Business.UserAccount.UserAccount;
import userinterface.AdministrativeRole.AdminWorkAreaJPanel;
import javax.swing.JPanel;
import userinterface.SupplierRole.SupplierAdminWorkAreaJPanel;

/**
 *
 * @author raunak
 */
public class AdminRole extends Role{

    @Override
    public JPanel createWorkArea(JPanel userProcessContainer, UserAccount account, Organization organization, Enterprise enterprise, EcoSystem business) {
        if(enterprise.getEnterpriseType().equals(Enterprise.EnterpriseType.Cinema)){
            return new AdminWorkAreaJPanel(userProcessContainer, enterprise, account, business, organization);
        }else if(enterprise.getEnterpriseType().equals(Enterprise.EnterpriseType.Supplier)){
            return new SupplierAdminWorkAreaJPanel(userProcessContainer, enterprise, account, business);
        }else{
            return null;
        }
    }

    
    
}
