/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Movie;

import java.util.ArrayList;

/**
 *
 * @author wangwangleyi
 */
public class TimeSectionDirctory {
    private ArrayList<TimeSection> tList;
    public TimeSectionDirctory(){
        tList = new ArrayList();
        for(int i = 0 ; i <7;i++){
           TimeSection t = new TimeSection();
           t.setId(i);
           tList.add(t);
        }
        tList.get(0).setTime("10:00-12:00");
        tList.get(1).setTime("12:00-14:00");
        tList.get(2).setTime("14:00-16:00");
        tList.get(3).setTime("16:00-18:00");
        tList.get(4).setTime("18:00-20:00");
        tList.get(5).setTime("20:00-22:00");
        tList.get(6).setTime("22:00-24:00");
        
    }

    public ArrayList<TimeSection> gettList() {
        return tList;
    }

    public void settList(ArrayList<TimeSection> tList) {
        this.tList = tList;
    }
    
}
