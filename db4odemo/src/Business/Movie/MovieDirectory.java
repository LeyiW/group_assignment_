/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Movie;

import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author lvyan
 */
public class MovieDirectory {
    private ArrayList<Movie> movieList_sysadmin;
    private ArrayList<Movie> result_movieList;
    

    public MovieDirectory() {
        movieList_sysadmin = new ArrayList(); 
        result_movieList = new ArrayList();
    }
    
    public ArrayList<Movie> getResult_movieList() {
        return result_movieList;
    }
    
     public ArrayList<Movie> getMovieList_sysadmin() {
        return movieList_sysadmin;
    }
     
    public Movie authenticateMovie(String movieName){
        for (Movie mo : movieList_sysadmin)
            if (mo.getMoviename().equals(movieName) ){
                return mo;
            }
        return null;
    }

     public void createMovie_sysadmin(Movie movie){
        
        movieList_sysadmin.add(movie);
        
    }
     
    public void createResult_movieList(Movie movie){
        
        result_movieList.add(movie);
        
    }
     
     public void createMovie_sysadmin(String movieName, int time, String director, Date showTime, Movie.MovieType type, int price ){
        Movie movie = new Movie();
        movie.setMoviename(movieName);
        movie.setDiretor(director);
        movie.setTime(time);
        movie.setType(type);
        movie.setShowTime(showTime);
        movie.setPrice(price);
        movieList_sysadmin.add(movie);
        
    }
    
    
    public boolean checkIfMovieNameIsUnique(String movieName){
        for (Movie mo : movieList_sysadmin){
            if (mo.getMoviename().equals(movieName))
                return false;
        }
        return true;
    }
}
