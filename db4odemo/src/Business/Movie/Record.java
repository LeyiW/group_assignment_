/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Movie;

import Business.Hall.Hall;
import Business.Hall.Seat;
import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author wangwangleyi
 */
public class Record {
   
    private String playDate;
    private Hall hall;
    private String hallName;
    private int hallId;
    private String timeSection;
    private ArrayList<Seat> seatList;

    public String getHallName() {
        return hallName;
    }

    public void setHallName(String hallName) {
        this.hallName = hallName;
    }

    public int getHallId() {
        return hallId;
    }

    public void setHallId(int hallId) {
        this.hallId = hallId;
    }
    

  
    public String getTimeSection() {
        return timeSection;
    }

    public void setTimeSection(String timeSection) {
        this.timeSection = timeSection;
    }

    public Hall getHall() {
        return hall;
    }

    public void setHall(Hall hall) {
        this.hall = hall;
    }

    public ArrayList<Seat> getSeatList() {
        return seatList;
    }

    public void setSeatList() {
        if(this.hall != null){
            seatList = new ArrayList<Seat>();
            for(int i = 0; i < this.hall.getSeatList().size() ; i++){
                Seat seat = this.hall.getSeatList().get(i);
                this.seatList.add(i, seat);
            }
        }
    }


    public String getPlayDate() {
        return playDate;
    }

    public void setPlayDate(String playDate) {
        this.playDate = playDate;
    }

    @Override
   public String toString(){
       return playDate;
   }
    
    
}
