/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Movie;

import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author lvyan
 */
public class Movie {
    
    private String Moviename;
    private double price;
    private int movieID;
    private static int count = 1;
    private int time;
    private String diretor;
    private Date showTime;
    
    private String reason;
    
    private Movie.MovieType type;
    private ArrayList<Record> recordList;
    
    private ArrayList<Integer> scoreList;
    private int score;
    //static int movieScoreSum ;
    private int movieScoreSum;

     public void score(Integer scoreIt) {
        scoreList.add(scoreIt);
        calculateScore();
    }
     
    public int getScore() {
        return score;
    }

    public void calculateScore() {
        movieScoreSum = 0;
        if(scoreList.size() != 0){
            for(int i = 0; i < scoreList.size(); i++){
                movieScoreSum += scoreList.get(i);
                this.score = movieScoreSum/scoreList.size();
            }  
        }
        else 
            this.score = 0;
    }

    public ArrayList<Integer> getScoreList() {
        return scoreList;
    }

    public void setScoreList(ArrayList<Integer> scoreList) {
        this.scoreList = scoreList;
    }
    
    public ArrayList<Record> getRecordList() {
        return recordList;
    }

    public void setRecordList(ArrayList<Record> recordList) {
        this.recordList = recordList;
    }
    
    

   

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }
    
    public Movie() {
        this.movieID = count;
        count ++;
        //movieScoreSum = 0;
        recordList = new ArrayList();
        scoreList = new ArrayList();
    }

    
   

    public enum MovieType{
        //Admin("Admin Organization"), Purchaser("Doctor Organization"), Dispatcher("Lab Organization");
        //drama剧情片
        Drama("Drama Film"), Documentary("Documentary Film"), Cartoon("Cartoon Film"), Musical("Musical Film");
        private String value;
        
        //constructor
        private MovieType(String value) {
            this.value = value;
        }
        
        public String getValue() {
            return value;
        }
    }

    public String getMoviename() {
        return Moviename;
        
    }

    public void setMoviename(String Moviename) {
        this.Moviename = Moviename;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getMovieID() {
        return movieID;
    }

    public void setMovieID(int movieID) {
        this.movieID = movieID;
    }

    public String getDiretor() {
        return diretor;
    }

    public void setDiretor(String diretor) {
        this.diretor = diretor;
    }

    public Date getShowTime() {
        return showTime;
    }

    public void setShowTime(Date showTime) {
        this.showTime = showTime;
    }
    
    public MovieType getType() {
        return type;
    }

    public void setType(MovieType type) {
        this.type = type;
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }

    public String toString(){
        return Moviename;
    }

}
