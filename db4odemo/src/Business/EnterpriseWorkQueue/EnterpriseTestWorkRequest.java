/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.EnterpriseWorkQueue;

import Business.Enterprise.Enterprise;


/**
 *
 * @author raunak
 */
public class EnterpriseTestWorkRequest extends EnterpriseWorkRequest{
    
    private String testResult;
    private Enterprise enterprise;

    public Enterprise getEnterprise() {
        return enterprise;
    }

    public void setEnterprise(Enterprise enterprise) {
        this.enterprise = enterprise;
    }

    
    
    public String getTestResult() {
        return testResult;
    }

    public void setTestResult(String testResult) {
        this.testResult = testResult;
    }
    
    
}
