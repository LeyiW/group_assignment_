/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.EnterpriseWorkQueue;

import java.util.ArrayList;

/**
 *
 * @author raunak
 */
public class EnterpriseWorkQueue {
    
    private ArrayList<EnterpriseWorkRequest> workRequestList;

    public EnterpriseWorkQueue() {
        workRequestList = new ArrayList();
    }

    public ArrayList<EnterpriseWorkRequest> getWorkRequestList() {
        return workRequestList;
    }
}