/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Organization;

import Business.Role.DispatcherRole;
import Business.Role.Role;
import java.util.ArrayList;

/**
 *
 * @author raunak
 */
public class DispatcherOrganization extends Organization{

    public DispatcherOrganization(String name) {
        super(Organization.Type.Dispatcher.getValue(), name);
    }

    @Override
    public ArrayList<Role> getSupportedRole() {
        ArrayList<Role> roles = new ArrayList();
        roles.add(new DispatcherRole());
        return roles;
    } 
}
