/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Organization;

import Business.Organization.Organization.Type;
import java.util.ArrayList;

/**
 *
 * @author raunak
 */
public class OrganizationDirectory {
    
    private ArrayList<Organization> organizationList;

    public OrganizationDirectory() {
        organizationList = new ArrayList();
    }

    public ArrayList<Organization> getOrganizationList() {
        return organizationList;
    }
    
    public Organization createOrganization(Type type, String name){
        Organization organization = null;
        if (type.getValue().equals(Type.Purchaser.getValue())){
            organization = new PurchaserOrganization(name);
            organizationList.add(organization);
        }
        else if (type.getValue().equals(Type.Dispatcher.getValue())){
            organization = new DispatcherOrganization(name);
            organizationList.add(organization);
        }else if (type.getValue().equals(Type.Admin.getValue())){
            organization = new AdminOrganization(name);
            organizationList.add(organization);
        }else if (type.getValue().equals(Type.SupplierAdmin.getValue())){
            organization = new SupplierAdminOrganization(name);
            organizationList.add(organization);
        }
        return organization;
    }
}