/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.AdminWorkQueue;

/**
 *
 * @author raunak
 */
public class AdminTestWorkRequest extends AdminWorkRequest{
    
    private String testResult;

    public String getTestResult() {
        return testResult;
    }

    public void setTestResult(String testResult) {
        this.testResult = testResult;
    }
    
    
}
