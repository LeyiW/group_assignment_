/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.UserAccount;

import Business.Employee.Employee;
import Business.Role.Role;
import Business.AdminWorkQueue.WorkQueue;
import Business.EnterpriseWorkQueue.EnterpriseWorkQueue;

/**
 *
 * @author raunak
 */
public class UserAccount extends Account{
    private Employee employee;
    private Role role;
    private WorkQueue workQueue;
    private EnterpriseWorkQueue enterpriseWorkQueue;

    public UserAccount() {
        workQueue = new WorkQueue();
        enterpriseWorkQueue = new EnterpriseWorkQueue();
    }

    public Role getRole() {
        return role;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public Employee getEmployee() {
        return employee;
    }

    public WorkQueue getWorkQueue() {
        return workQueue;
    }
    
     public EnterpriseWorkQueue getEnterpriseWorkQueue() {
        return enterpriseWorkQueue;
    }

}