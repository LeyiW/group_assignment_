/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.UserAccount;

import Business.EcoSystem;
import Business.Movie.Movie;
import java.util.ArrayList;
import javax.swing.JPanel;
import userinterface.CustomerRole.CustomerWorkAreaJPanel;

/**
 *
 * @author lvyan
 */
public class CustomerAccount extends Account{
    ArrayList<Movie> movieList;

    public ArrayList<Movie> getMovieList() {
        return movieList;
    }
    public CustomerAccount(){
        movieList = new ArrayList<Movie>();
    }
    
    
    public JPanel createWorkArea(JPanel userProcessContainer, CustomerAccount account, EcoSystem business) {
        return new CustomerWorkAreaJPanel(userProcessContainer, account, business);
    }
}
