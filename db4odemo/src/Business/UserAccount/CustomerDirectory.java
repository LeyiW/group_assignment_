/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.UserAccount;

import java.util.ArrayList;

/**
 *
 * @author lvyan
 */
public class CustomerDirectory {
    
    private ArrayList<CustomerAccount> customerAccountList;
    
    public CustomerDirectory() {
        customerAccountList = new ArrayList();
    }

    public ArrayList<CustomerAccount> getCustomerAccountList() {
        return customerAccountList;
    }

    public CustomerAccount authenticateUser(String username, String password){
        for (CustomerAccount ua : customerAccountList)
            if (ua.getUsername().equals(username) && ua.getPassword().equals(password)){
                return ua;
            }
        return null;
    }
    
    public CustomerAccount createUserAccount(String username, String password){
        CustomerAccount userAccount = new CustomerAccount();
        userAccount.setUsername(username);
        userAccount.setPassword(password);
        customerAccountList.add(userAccount);
        return userAccount;
    }
    
    public boolean checkIfUsernameIsUnique(String username){
        for (CustomerAccount ua : customerAccountList){
            if (ua.getUsername().equals(username))
                return false;
        }
        return true;
    }
}
