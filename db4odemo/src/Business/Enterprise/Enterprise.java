 /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Enterprise;

import Business.EnterpriseWorkQueue.EnterpriseWorkQueue;
import Business.Hall.HallDirectory;
import Business.Movie.MovieDirectory;
import Business.Organization.Organization;
import Business.Organization.OrganizationDirectory;

/**
 *
 * @author MyPC1
 */
public abstract class Enterprise extends Organization{
    
    private EnterpriseType enterpriseType;
    private OrganizationDirectory organizationDirectory;
    private HallDirectory hallDirectory;
    private MovieDirectory movieDirectory;
    private EnterpriseWorkQueue enterpriseWorkQueue;
    
    public OrganizationDirectory getOrganizationDirectory() {
        return organizationDirectory;
    }
    
    public enum EnterpriseType{
        Cinema("Cinema"), Supplier("Supplier");
        
        private String value;
        
        private EnterpriseType(String value){
            this.value=value;
        }
        public String getValue() {
            return value;
        }
        @Override
        public String toString(){
            return value;
        }
    }

    public EnterpriseType getEnterpriseType() {
        return enterpriseType;
    }

    public void setEnterpriseType(EnterpriseType enterpriseType) {
        this.enterpriseType = enterpriseType;
    }

    public HallDirectory getHallDirectory() {
        return hallDirectory;
    }

    public void setHallDirectory(HallDirectory hallDirectory) {
        this.hallDirectory = hallDirectory;
    }

    public MovieDirectory getMovieDirectory() {
        return movieDirectory;
    }

    public void setMovieDirectory(MovieDirectory movieDirectory) {
        this.movieDirectory = movieDirectory;
    }

    public EnterpriseWorkQueue getEnterpriseWorkQueue() {
        return enterpriseWorkQueue;
    }

    public void setEnterpriseWorkQueue(EnterpriseWorkQueue enterpriseWorkQueue) {
        this.enterpriseWorkQueue = enterpriseWorkQueue;
    }

    
   
    
    
    public Enterprise(String name,EnterpriseType type){
        super(String.valueOf(type),name);
        this.enterpriseType=type;
        movieDirectory = new MovieDirectory();
        enterpriseWorkQueue = new EnterpriseWorkQueue();
        organizationDirectory=new OrganizationDirectory();
        hallDirectory = new HallDirectory();
    }
}