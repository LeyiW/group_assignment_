/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment_4;

import assignment_4.analytics.AnalysisHelper;
import assignment_4.analytics.DataStore;
import assignment_4.entities.Customer;
import assignment_4.entities.Item;
import assignment_4.entities.Order;
import assignment_4.entities.Product;
import assignment_4.entities.SalesPerson;
import java.io.IOException;
import java.util.Map;

/**
 *
 * @author harshalneelkamal
 */
public class GateWay {
    DataReader orderReader;
    DataReader productReader;
    DataGenerator generator;
    AnalysisHelper helper;
    
    public GateWay() throws IOException {
        generator = DataGenerator.getInstance();
        orderReader = new DataReader(generator.getOrderFilePath());
        productReader = new DataReader(generator.getProductCataloguePath());
        helper = new AnalysisHelper();
    }
    
    public static void main(String[] args) throws IOException { 
        
        GateWay inst = new GateWay();
        inst.readData();
    }
    
    private void readData() throws IOException{
        String[] row;
        while((row = orderReader.getNextRow()) != null ){
            Item it = generateItem(row);
            Order o = generateOrder(row,it);
            generateCustomer(row,o);
            generateSalesPerson(row,o);
        }
        while((row = productReader.getNextRow()) != null ){
            generateProduct(row);
        }
        
        runAnalysis();
    }
    private Item generateItem(String[] row){
        int itemId = Integer.parseInt(row[1]);
        int productId = Integer.parseInt(row[2]);
        int salesPrice = Integer.parseInt(row[6]);
        int quantity = Integer.parseInt(row[3]);
        Item i = new Item(itemId, productId, salesPrice,quantity);
        DataStore.getInstance().getItem().put(itemId,i);
        return i;
    }
    
    private void generateCustomer(String[] row, Order o){
        int customerId = Integer.parseInt(row[5]);
        Map<Integer, Customer> customers = DataStore.getInstance().getCustomer();
        if(customers.containsKey(customerId))
            customers.get(customerId).getOrders().add(o);
        else{
            Customer c = new Customer(customerId);
            c.getOrders().add(o);
            customers.put(customerId, c);
            
        }
    }
    
    private void generateSalesPerson(String[] row,Order o){
    int salesId = Integer.parseInt(row[4]);
    Map<Integer, SalesPerson> salesPersons = DataStore.getInstance().getSalesPerson();
        if(salesPersons.containsKey(salesId))
            salesPersons.get(salesId).getOrder().add(o);
        else{
            SalesPerson sp = new SalesPerson(salesId);
            sp.getOrder().add(o);
            salesPersons.put(salesId, sp);
        }
    }
    
     private Order generateOrder(String[] row, Item i){
        int orderId = Integer.parseInt(row[0]);
        int salesId = Integer.parseInt(row[4]);
        int customerId = Integer.parseInt(row[5]);
        Order o = new Order(orderId, salesId, customerId,i);
        DataStore.getInstance().getOrder().put(orderId,o);
        return o;
    }
    
    private void generateProduct(String[] row){
        int productId = Integer.parseInt(row[0]);
        int min_price = Integer.parseInt(row[1]);
        int max_price = Integer.parseInt(row[2]);
        int target_price = Integer.parseInt(row[3]);
        Product p = new Product(productId, min_price, max_price , target_price);
        DataStore.getInstance().getProduct().put(productId,p);
    }
    
    
    
    private void runAnalysis(){
        //在这里执行analysis的函数 
        helper.topThreePopularProduct();
        helper.topThreeCustomer();
        helper.topThreeSalesPerson();
        helper.anualRevenue();
        //helper.forTest();
        
    }
    
}
