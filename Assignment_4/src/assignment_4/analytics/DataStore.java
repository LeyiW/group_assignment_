/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment_4.analytics;

import assignment_4.entities.Customer;
import assignment_4.entities.Item;
import assignment_4.entities.Order;
import assignment_4.entities.Product;
import assignment_4.entities.SalesPerson;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author harshalneelkamal
 */
//用hashmap储存信息，这样就能把class连起来而不用class association了
//hashmap是键值对，可以通过key查找对应的object
//single model：只保留一个副本，可以在任何地方使用它

public class DataStore {
    
    private static DataStore dataStore;
    
    private Map<Integer, Customer> customer;
    private Map<Integer, Item> item;
    private Map<Integer, Order> order;
    private Map<Integer, Product> Product;
    private Map<Integer, SalesPerson> salesPerson;
    
    private DataStore(){
        customer = new HashMap<>();
        item = new HashMap<>();
        order = new HashMap<>();
        Product = new HashMap<>();
        salesPerson = new HashMap<>();
    }
    
    public static DataStore getInstance(){
        if(dataStore == null)
            dataStore = new DataStore();
        return dataStore;
    }

    public static DataStore getDataStore() {
        return dataStore;
    }

    public static void setDataStore(DataStore dataStore) {
        DataStore.dataStore = dataStore;
    }

    public Map<Integer, Customer> getCustomer() {
        return customer;
    }

    public void setCustomer(Map<Integer, Customer> customer) {
        this.customer = customer;
    }

    public Map<Integer, Item> getItem() {
        return item;
    }

    public void setItem(Map<Integer, Item> item) {
        this.item = item;
    }

    public Map<Integer, Order> getOrder() {
        return order;
    }

    public void setOrder(Map<Integer, Order> order) {
        this.order = order;
    }

    public Map<Integer, Product> getProduct() {
        return Product;
    }

    public void setProduct(Map<Integer, Product> Product) {
        this.Product = Product;
    }

    public Map<Integer, SalesPerson> getSalesPerson() {
        return salesPerson;
    }

    public void setSalesPerson(Map<Integer, SalesPerson> salesPerson) {
        this.salesPerson = salesPerson;
    }

    

}
