/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment_4.analytics;

import assignment_4.entities.Customer;
import assignment_4.entities.Item;
import assignment_4.entities.Order;
import assignment_4.entities.Product;
import assignment_4.entities.SalesPerson;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author lvyan
 */
public class AnalysisHelper {
    //wly
    public void topThreePopularProduct(){
        Map<Integer, Order> orders = DataStore.getInstance().getOrder();
        Map<Integer, Product> products = DataStore.getInstance().getProduct();
        Map<Integer, Integer> productQCount = new HashMap<>();
        
        //找到每个oder里的 item对应的 product id 把quality加在value里       
        for(Order order: orders.values()){
            int quantity = order.getItem().getQuantity();
            int productId = order.getItem().getProductId();
            //for(Product product : products.values()){
               // if(productId == product.getProductId()){
                   // quantity = order.getItem().getQuantity();
                   // break;
                //}
        //}
            if(!productQCount.containsKey(productId)){
                productQCount.put(productId, quantity);
                
            }else{
                productQCount.put(productId, productQCount.get(productId) + quantity);
            }
        }
        
        //sort
        List<Map.Entry<Integer,Integer>> productQCountList=new ArrayList<>();

        productQCountList.addAll(productQCount.entrySet());
        ValueComparator vc=new ValueComparator();
        Collections.sort(productQCountList,vc);

       
        
        System.out.println("  top 3 popular Product: ");
        for(int i = productQCountList.size() -1; i >= 0&& i > productQCountList.size() -4; i--){
            System.out.println("ProductID = "+productQCountList.get(i).getKey()+" Sell: "+productQCountList.get(i).getValue()); 
        }
    }
    
    //wly
    public void topThreeCustomer(){
        //花钱最多的顾客...
        Map<Integer, Order> orders = DataStore.getInstance().getOrder();
        Map<Integer, Customer> customers = DataStore.getInstance().getCustomer();
        Map<Integer, Integer> customerSpendCount = new HashMap<>();
        
        //找到每个order里的customer 对应item 里的 salesPrice 和 quantity
        for(Order order: orders.values()){
            
            int customerId = order.getCustomerId();
            int quantity = order.getItem().getQuantity();
            int salePrice = order.getItem().getSalesPrice();   
            int customerSpend = salePrice * quantity;
            if(!customerSpendCount.containsKey(customerId)){
                customerSpendCount.put(customerId, customerSpend);
                
            }else{
                customerSpendCount.put(customerId, customerSpendCount.get(customerId) + customerSpend);
            }
        }
        
        //sort
        List<Map.Entry<Integer,Integer>> customerSpendCountList=new ArrayList<>();

        customerSpendCountList.addAll(customerSpendCount.entrySet());
        ValueComparator vc=new ValueComparator();
        Collections.sort(customerSpendCountList,vc);

       
        
        System.out.println("  top 3 Best customer(金主爸爸): ");
        for(int i = customerSpendCountList.size() -1; i >= 0&& i > customerSpendCountList.size() -4; i--){
            System.out.println("CustomerID = "+customerSpendCountList.get(i).getKey()+" Spend: "+customerSpendCountList.get(i).getValue()); 
        }
    
    }

    //cyj
    public void topThreeSalesPerson(){
        
        Map<Integer, Order> orders = DataStore.getInstance().getOrder();
        Map<Integer, Product> products = DataStore.getInstance().getProduct();
        Map<Integer, Integer> salePersonCount = new HashMap<>();
        //找到每一个order里对应的sale person， 对应的item， 对应的quantity和price和product id， 并找到product id对应的targetprice
        for(Order order: orders.values()){
            int saleId = order.getSalesId();
            int quantity = order.getItem().getQuantity();
            int salePrice = order.getItem().getSalesPrice();
            int productId = order.getItem().getProductId();
            int targetPrice = 0;
            for(Product product : products.values()){
                if(productId == product.getProductId()){
                    targetPrice = product.getTarget_price();
                    break;
                }
            }
            int totalPrice = (salePrice - targetPrice) * quantity;
            if(!salePersonCount.containsKey(saleId)){
                salePersonCount.put(saleId, totalPrice);
                
            }else{
                salePersonCount.put(saleId, salePersonCount.get(saleId) + totalPrice);
            }
        }
         //Sort
        List<Map.Entry<Integer,Integer>> salePersonCountList=new ArrayList<>();

        salePersonCountList.addAll(salePersonCount.entrySet());
        ValueComparator vc=new ValueComparator();
        Collections.sort(salePersonCountList,vc);

       
        
        System.out.println("  top 3 best sale Person: ");
        for(int i = salePersonCountList.size() -1; i >= 0&& i > salePersonCountList.size() -4; i--){
            System.out.println("SaleID = "+salePersonCountList.get(i).getKey()+" Total Price "+salePersonCountList.get(i).getValue()); 
        }
        
       
        
        
    
    }
    
    //cyj
    public void anualRevenue(){
         
        Map<Integer, Order> orders = DataStore.getInstance().getOrder();
        Map<Integer, Product> products = DataStore.getInstance().getProduct();
        
        int revenuePrice = 0;
        for(Order order: orders.values()){
            int saleId = order.getSalesId();
            int quantity = order.getItem().getQuantity();
            int salePrice = order.getItem().getSalesPrice();
            int productId = order.getItem().getProductId();
            int minPrice = 0;
            for(Product product : products.values()){
                if(productId == product.getProductId()){
                    minPrice = product.getMin_price();
                    break;
                }
            }
            int totalPrice = (salePrice - minPrice) * quantity;
            revenuePrice += totalPrice;
        }
        
         System.out.println("  The revenue for the year is:  " + revenuePrice);
    
    }
    
    private class ValueComparator implements Comparator<Map.Entry<Integer,Integer>>  
     {  
        public int compare(Map.Entry<Integer, Integer> mp1, Map.Entry<Integer, Integer> mp2)   
         {  
             return mp1.getValue() - mp2.getValue();  
         }  
     }
    
//    public void forTest(){
//        System.out.println("here is test");
//        //customer
//        //!!!order的list
//        Map<Integer, Customer> customers = DataStore.getInstance().getCustomer();
//        List<Customer> customerList = new ArrayList<>(customers.values());
//        Collections.sort(customerList, new Comparator<Customer>(){
//            public int compare(Customer o1, Customer o2){
//                return o2.getCustomerId() - o1.getCustomerId();
//            }  
//    }); 
//        System.out.println("5 customerList :");
//        for(int i = 0; i < customerList.size() && i < 5; i++){
//            System.out.print(customerList.get(i));
//            System.out.println("   Order size is :"+customerList.get(i).getOrders().size());
//        }
//        
//        //Item
//        Map<Integer, Item> items = DataStore.getInstance().getItem();
//        List<Item> itemList = new ArrayList<>(items.values());
//        Collections.sort(itemList, new Comparator<Item>(){
//            public int compare(Item o1, Item o2){
//                return o2.getItemId() - o1.getItemId();
//            }  
//    }); 
//        System.out.println("5 itemList :");
//        for(int i = 0; i < itemList.size() && i < 5; i++){
//            System.out.println(itemList.get(i));
//        }
//        
//        //Order
//        Map<Integer, Order> orders = DataStore.getInstance().getOrder();
//        List<Order> orderList = new ArrayList<>(orders.values());
//        Collections.sort(orderList, new Comparator<Order>(){
//            public int compare(Order o1, Order o2){
//                return o2.getOrderId() - o1.getOrderId();
//            }  
//    }); 
//        System.out.println("5 orderList :");
//        for(int i = 0; i < orderList.size() && i < 5; i++){
//            System.out.println(orderList.get(i));
//        }
//        
//        //product
//        Map<Integer, Product> products = DataStore.getInstance().getProduct();
//        List<Product> productList = new ArrayList<>(products.values());
//        Collections.sort(productList, new Comparator<Product>(){
//            public int compare(Product o1, Product o2){
//                return o2.getProductId() - o1.getProductId();
//            }  
//    }); 
//        System.out.println("5 productList :");
//        for(int i = 0; i < productList.size() && i < 5; i++){
//            System.out.println(productList.get(i));
//        }
//        
//        
//         //sales person
//        //!!!order的list
//        Map<Integer, SalesPerson> salesPersons = DataStore.getInstance().getSalesPerson();
//        List<SalesPerson> salesPersonList = new ArrayList<>(salesPersons.values());
//        Collections.sort(salesPersonList, new Comparator<SalesPerson>(){
//            public int compare(SalesPerson o1, SalesPerson o2){
//                return o2.getSalesId() - o1.getSalesId();
//            }  
//    }); 
//        System.out.println("5 salesPersonList :");
//        for(int i = 0; i < salesPersonList.size() && i < 5; i++){
//            System.out.print(salesPersonList.get(i));
//            System.out.println("   Order size is :"+salesPersonList.get(i).getOrder().size());
//        }
//    }
//    

}
