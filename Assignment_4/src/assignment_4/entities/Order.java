/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment_4.entities;

/**
 *
 * @author harshalneelkamal
 */
public class Order {
    private int orderId;
    private int salesId;
    private int customerId;
    private Item item;

    public Order(int orderId, int salesId, int customerId, Item item) {
        this.orderId = orderId;
        this.salesId = salesId;
        this.customerId = customerId;
        this.item = item;
    }

    public int getSalesId() {
        return salesId;
    }

    public void setSalesId(int salesId) {
        this.salesId = salesId;
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }
    
    @Override
    public String toString(){
        return "Order{ id = " + orderId + " Sales id = " + salesId+ "customer id = "+customerId+ "item = "+item +'}';
    }
}
